package com.example.multiscreenapp

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class ForthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_4)

        Log.i("log_activity_4", "onCreate")

        val buttonNext = findViewById<Button>(R.id.buttonNext1)
        buttonNext.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        val buttonPrev = findViewById<Button>(R.id.buttonPrev3)
        buttonPrev.setOnClickListener {
            val intent = Intent(this, ThirdActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()
        Log.i("log_activity_4", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.i("log_activity_4", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.i("log_activity_4", "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i("log_activity_4", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("log_activity_4", "onDestroy")
    }
}