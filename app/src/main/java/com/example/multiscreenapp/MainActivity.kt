package com.example.multiscreenapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.i("log_activity_main", "onCreate")

        val buttonNext = findViewById<Button>(R.id.buttonNext2)
        buttonNext.setOnClickListener{
            val intent = Intent(this, SecondActivity::class.java)
            startActivity(intent)
        }
        val buttonPrev = findViewById<Button>(R.id.buttonPrev0)
        buttonPrev.setOnClickListener{
            finishAffinity() //ask finish()
        }
    }

    override fun onStart() {
        super.onStart()
        Log.i("log_activity_main", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.i("log_activity_main", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.i("log_activity_main", "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i("log_activity_main", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("log_activity_main", "onDestory")
    }
}