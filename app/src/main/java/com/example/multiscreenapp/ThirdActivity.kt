package com.example.multiscreenapp

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class ThirdActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_3)

        Log.i("log_activity_3", "onCreate")

        val buttonNext = findViewById<Button>(R.id.buttonNext4)
        buttonNext.setOnClickListener {
            val intent = Intent(this, ForthActivity::class.java)
            startActivity(intent)
        }

        val buttonPrev = findViewById<Button>(R.id.buttonPrev2)
        buttonPrev.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            startActivity(intent)
        }
    }
    override fun onStart() {
        super.onStart()
        Log.i("log_activity_3", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.i("log_activity_3", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.i("log_activity_3", "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i("log_activity_3", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("log_activity_3", "onDestroy")
    }
}