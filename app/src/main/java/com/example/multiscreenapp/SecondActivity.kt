package com.example.multiscreenapp

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class SecondActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_2)

        Log.i("log_activity_2", "onCreate")

        val buttonNext = findViewById<Button>(R.id.buttonNext3)
        buttonNext.setOnClickListener {
            val intent2 = Intent(this, ThirdActivity::class.java)
            startActivity(intent2)
        }

        val buttonPrev = findViewById<Button>(R.id.buttonPrev1)
        buttonPrev.setOnClickListener {
            val intent3 = Intent(this, MainActivity::class.java)
            startActivity(intent3)
        }
    }
    override fun onStart() {
        super.onStart()
        Log.i("log_activity_2", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.i("log_activity_2", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.i("log_activity_2", "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i("log_activity_2", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("log_activity_2", "onDestroy")
    }

}